# Laboratoire 5 : Chercher, filtrer couper

## Exercice 1 : Dictionnaires de mots (30 mins)

### 1 : Récupérer une liste de mots

### 2 : Questions

1. Combien de mots contient le dictionnaire?

```sh
wc -l words
```
102305 words

2. Combien de mots contiennent la chaîne with

```sh
grep -c 'with' words
```
35

3. Combien de mots commencent par la chaîne with?

```sh
grep -c "^with" words
```
27

4. Combien de mots terminent par la chaîne with?

```sh
grep -c 'with$' words
```
5

```sh
grep 'with$' words
```
Sopwith
forthwith
herewith
therewith
with

5. Combien de mots terminent par 's?

```sh
grep -c "'s" words
```
28980

6. Placez tous les mots qui commencent par la lettre majuscule T et placez-les
dans un fichier nommé T.txt en les répartissant sur 4 colonnes.

```sh
grep "^T" words | column > ~/uqam/inf1070/inf1070-labos/T.txt
```

```sh
grep "^T" words | pr -t --columns=4 > ~/uqam/inf1070/inf1070-labos/T.txt
```

7. Placez tous les noms propres (mots qui commencent par une majuscule) et
placez-les dans un fichier nommé noms-propres.txt. Combien y en a-t-il?

```sh
grep '^[A-Z]' words > ~/uqam/inf1070/inf1070-labos/noms-propres.txt
```

```sh
grep -c '^[A-Z]' words
```
19112


Exercice 2.4

1. Quels sont les codes de 3 lettres des 10 pays dont la population est la plus
importante?

```sh
grep -v '^#' country-clean.txt | cut -f2,8 |sort -k 2 -nr | head
```

2. Quels sont les codes de 3 lettres des 20 pays dont la superficie est la plus
grande?

```sh
grep -v'^#' country-clean.txt | cut -f2,8 |sort -k 2 -nr | head -n 20
```

3. Combien existe-t-il de monnaie officielle différente sur notre planète?

```sh
grep -v '^#' country-clean.txt | cut -f11 | wc -l
```
252

```sh
grep -v '^#' country-clean.txt | cut -f11 | sort | uniq | wc -l
```
156

4.  Combien y a-t-il de pays par continent?

```sh
grep -v '^#' country-clean.txt | cut -f9 | sort | uniq -c
```
252

Exercice 2.5

1. Quels sont les fuseaux horaires possibles au Canada? Combien y en a-t-il?

```sh
cut -f18 CA-clean.txt | sort | uniq | grep "America"
```

```sh
cut -f18 CA-clean.txt | sort | uniq | grep -c "America"
```
38

2. Quels sont les lieux qui contiennent la chaîne Longueuil? Combien y en
a-t-il?

```sh
grep 'Longueuil' CA-clean.txt
```

```sh
grep -c 'Longueuil' CA-clean.txt 
```
17

3.  Quelles sont les données qui ont été modifiées en 2018?

```sh
cut -f19 CA-clean.txt | grep -c '2018'
```
28658

4. Quelles sont les longitudes minimale et maximale, ainsi que les latitudes
minimale et maximale des lieux décrits dans le fichier CA.txt?

```sh
cut -f6 CA-clean.txt | sort -n | head -n 1
```
-141.00542

```sh
cut -f6 CA-clean.txt | sort -nr | head -n 1
```
-48.5

```sh
cut -f5 CA-clean.txt | sort -n | head -n 1
```
41.65444

```sh
cut -f5 CA-clean.txt | sort -nr | head -n 1
```
83.12224

