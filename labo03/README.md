# Laboratoire 3 : Installer une machine virtuelle

## Exercice 1 : Installation d'une machine virtuelle (60 mins)

Une machine virtuelle est une application dans laquelle vous pouvez simuler un
système d'exploitation tel que Ubuntu même si votre machine tourne avec un
autre système tel que Windows ou MacOS. Il existe différentes machines
virtuelles disponibles gratuitement (VirtualBox, VMWare, etc.). Nous nous
concentrons sur VirtualBox, qui est libre en plus d'être gratuit.

Pour ce laboratoire, il est recommandé d'apporter votre propre ordinateur afin
d'installer une distribution Linux que vous pourrez utiliser à la maison. Si
vous n'avez pas de telle machine, vous pouvez tout de même installer une
machine virtuelle d'une autre distribution pour passer à travers les
différentes étapes. Il est très pratique de connaître le processus puisque vous
aurez à le faire probablement plus tard.

## 1.1 : Vérifier les prérequis

Tout d'abord, authentifiez-vous sur une machine qui tourne sous Windows (ou
MacOS, si vous avez un portable).

Rendez-vous sur [VirtualBox](https://www.virtualbox.org/).

Assurez-vous d'avoir tous les
[prérequis](https://www.virtualbox.org/manual/ch01.html#hostcpurequirements)
pour installer une machine virtuelle si vous utilisez votre machine
personnelle.

En tout temps, n'hésitez pas à consulter le
[manuel](https://www.virtualbox.org/manual/) expliquant les différentes étapes
d'installation d'une machine virtuelle.

## 1.2 : Lancer l'installation

Rendez-vous sur la section des
[téléchargements](https://www.virtualbox.org/wiki/Downloads) et choisissez la
version correspondant à la machine sur laquelle vous travaillez.

## 1.3 : Création d'une machine virtuelle

En consultant le manuel officiel, créez une machine virtuelle :

- Si c'est la première fois que vous utilisez un environnement Linux, nous vous
  recommandons d'installer une distribution Ubuntu ou Linux Mint.
- Si vous êtes sur les machines des laboratoires, alors vous pouvez installer
  une distribution Linux différente d'Ubuntu, par exemple Linux Mint, afin de
  comparer les deux distributions.

L'installation devrait prendre un certain temps. N'hésitez pas à consulter les
démonstrateurs si vous êtes bloqués à une des étapes.

Lorsque l'installation sera complétée, lancez la machine virtuelle, ouvrez un
terminal et vérifiez que tout fonctionne bien en entrant votre commande
préférée.

## Exercice 2 : Installation de logiciels (20 mins)

Installez les logiciels suivants en ligne de commande, à l'aide de l'utilitaire
`apt` (au besoin, utilisez `man apt` ou `apt search <nom programme>` pour
chercher un programme spécifique :

- Les programmes `cowsay`, `cmatrix`, `lolcat` vus en classe
- [Git](https://git-scm.com/)
- [Graphviz](https://www.graphviz.org/)
- [Pandoc](https://pandoc.org/)
- [Vim](https://www.vim.org/)

Le programme `apt-file` s'avère extrêment utile si on connait le nom de
l'exécutable ou d'un fichier contenu dans le paquet sans connaître d'information
assez précise sur le nom du paquet ou sa description.

## Exercice 3 : Installation de KDE (30 mins)

Par défaut, Ubuntu installe l'environnement de bureau
[Gnome](https://www.gnome.org/). Les anciennes versions utilisaient
l'environnement [Unity](https://unity8.io/).

L'objectif de cet exercice est d'installer l'environnement de bureau
[KDE](https://www.kde.org/). Pour cela, il suffit d'entrer la commande
```sh
sudo apt-get install kubuntu-desktop
```
puis de vous déconnecter (*log out*). Lorsque vous vous reconnecterez, vous
pouvez alors choisir l'environnement KDE.

Prenez le temps de bien vous familiariser avec la nouvelle interface :

* Lancez un terminal.
* Naviguez dans le système de fichier.
* Paramètres systèmes (vitesse de la souris, résolution de l'écran, clavier
  utilisé, image d'arrière-plan).
